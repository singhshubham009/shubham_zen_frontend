import React, { Component } from 'react';
import ZenStats from './containers/ZenStats/ZenStats'
class App extends Component {
  render() {
    return (
      <div className="App">
      <ZenStats/>
      </div>
    );
  }
}

export default App;
