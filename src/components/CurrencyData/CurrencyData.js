import React, {Component} from 'react';
import { Line } from "react-chartjs-2";


class CurrencyData extends Component{


  render(){
    let graph_data =null
    if (this.props.data&&this.props.show&&this.props.current===this.props.id) {
      graph_data = (
        <tr>
          <td colSpan="8">
            <Line
              data={this.props.data}
              height={200}
              options={{
                maintainAspectRatio: false
              }}
            />
          </td>
        </tr>
      );
    }
    return(
      <React.Fragment>
      <tr onClick={this.props.clicked}>
      <td>{this.props.name}</td>
      <td>{this.props.change}</td>
      <td>{this.props.market}</td>
      <td>{this.props.usdPrice}</td>
      <td>{this.props.supply}</td>
      <td>{this.props.volume}</td>
      </tr>      
      {graph_data}
      </React.Fragment>
      );
  }
}


export default CurrencyData