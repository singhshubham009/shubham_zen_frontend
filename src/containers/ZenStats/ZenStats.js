import React, { Component } from "react";
import axios from "axios";
import CurrencyData from "../../components/CurrencyData/CurrencyData";


class ZenStats extends Component {
  state = {
    data: null,
    clicked:false,
    index: null,
    currency_id: null,
    chartData: {
      labels: [],
      datasets: [
        {
          label: "Price",
          data: null,
          backgroundColor: "blue"
        }
      ]
    }
  };

  getGraphData = (id) => {
    this.setState({currency_id: id , clicked:false});
    const data = {
      currency_id: id
    }
    axios.post("http://127.0.0.1:8000/api/content/", data).then(response => {
      let time = [];
      let price = [];
      const chartData = { ...this.state.chartData };
      for (let key in response.data.items[0].monthly) {
        let d = new Date(0);
        d.setUTCSeconds(key);
        if (time.includes(d.getDate()) === false) {
          time.push(d.getDate());
          price.push(response.data.items[0].monthly[key][0]);
        }
      }
      chartData.datasets[0].data = [...price];
      chartData.labels = [...time];
      this.setState({ chartData: chartData ,clicked:true});
    });
  };

  componentDidMount() {
    axios
      .get("http://127.0.0.1:8000/api/content/")
      .then(response => {
        let data = [...response.data.items];
        this.setState({ data: data });
      })
      .catch(error => {
        alert("something is wrong");
      });
  }


  render() {
    let currency_data = null;

    if (this.state.data) {
      currency_data = this.state.data.map((currency, index) => {

        return (
          <CurrencyData
            name={currency.name}
            change={currency.percent_change_24h}
            market={currency.market_cap_usd}
            usdPrice={currency.price_usd}
            supply={currency.total_supply}
            volume={currency.volume_usd}
            key={currency.id + index}
            id={currency.id}
            current={this.state.currency_id}
            show={this.state.clicked}
            data={this.state.chartData}
            clicked={()=>{this.getGraphData(currency.id)}}
          />
        )
      })
    }
    return (
      <React.Fragment>
        <div className="container">
          <table className="table table-hover">
            <thead>
              <tr>
                <td>Currency</td>
                <td>Change % 24h</td>
                <td>Market Cap</td>
                <td>Price USD</td>
                <td>Supply</td>
                <td>Volume</td>
              </tr>
            </thead>
            <tbody>
            {currency_data}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default ZenStats;
